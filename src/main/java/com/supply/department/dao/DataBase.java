package com.supply.department.dao;

import com.supply.department.entity.*;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class DataBase {

    private Connection connection;

    private QueryRunner queryRunner = new QueryRunner();

    public DataBase(Connection connection) {
        this.connection = connection;
    }

    public List<Request> requestsGetAll() throws SQLException {
        return queryRunner.query(connection, "SELECT * FROM request;", new BeanListHandler<>(Request.class));
    }

    public boolean requestsAdd(Request request) throws SQLException {
        return queryRunner.insert(connection, "INSERT INTO request(department, info) VALUES(?,?);", new ScalarHandler<Long>(), request.getDepartment(), request.getInfo())>0;
    }

    public List<Material> materialsGetAll() throws SQLException {
        return queryRunner.query(connection, "SELECT c.materialGroup AS name, (SELECT IF(NULLIF(e.id, NULL), c.count - SUM(e.count), c.count) FROM expenditure AS e WHERE e.comingId = c.id) AS count FROM coming AS c;", new BeanListHandler<>(Material.class));
    }

    public List<Coming> comingGetAll() throws SQLException {
        return queryRunner.query(connection, "SELECT * FROM coming;", new BeanListHandler<>(Coming.class));
    }

    public List<Coming> comingGetAllWithExpenditure() throws SQLException {
        return queryRunner.query(connection, "SELECT c.*, (SELECT IF(NULLIF(e.id, NULL), c.count - SUM(e.count), c.count) FROM expenditure AS e WHERE e.comingId = c.id) AS freeCount FROM coming AS c;", new BeanListHandler<>(Coming.class));
    }

    public List<Remainder> remainsGetAll() throws SQLException {
        return queryRunner.query(connection, "SELECT c.*, (SELECT IF(NULLIF(e.id, NULL), c.count - SUM(e.count), c.count) FROM expenditure AS e WHERE e.comingId = c.id) AS freeCount FROM coming AS c;", new BeanListHandler<>(Remainder.class));
    }

    public boolean comingAdd(Coming coming) throws SQLException {
        return queryRunner.insert(connection, "INSERT INTO `coming` (`date`, `idDoc`, `powerOfAttorney`, `fio`, `provider`, `name`, `materialGroup`, `unit`, `count`, `price`, `amount`, `purchaseCode`, `accountingGroup`, `extraOptions`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);", new ScalarHandler<Long>(),
                coming.getDate(), coming.getIdDoc(), coming.getPowerOfAttorney(), coming.getFio(), coming.getProvider(), coming.getName(),
                coming.getMaterialGroup(), coming.getUnit(), coming.getCount(), coming.getPrice(), coming.getAmount(), coming.getPurchaseCode(), coming.getAccountingGroup(), coming.getExtraOptions())>0;
    }

    public List<Expenditure> expenditureGetAll() throws SQLException {
        return queryRunner.query(connection, "SELECT * FROM expenditure;", new BeanListHandler<>(Expenditure.class));
    }

    public boolean expenditureAdd(Expenditure e) throws SQLException {
        return queryRunner.insert(connection, "INSERT INTO `expenditure`(`comingId`, `date`, `basis`, `department`, `fio`, `name`, `materialGroup`, `unit`, `count`, `price`, `amount`, `purchaseCode`, `accountingGroup`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);", new ScalarHandler<Long>(),
                e.getComingId(), e.getDate(), e.getBasis(), e.getDepartment(), e.getFio(), e.getName(), e.getMaterialGroup(), e.getUnit(), e.getCount(),
                e.getPrice(), e.getAmount(), e.getPurchaseCode(), e.getAccountingGroup())>0;
    }

}
