package com.supply.department.dao;

import com.supply.department.datasource.config.DataBaseConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DataSource {

    private Connection connection;

    private DataBaseConfig config;

    private static DataSource dataBase;

    private DataSource(DataBaseConfig config) throws SQLException {
        //Class.forName(config.getDriverClassName());

        this.config = config;

        Properties connectionProps = new Properties();
        connectionProps.put("user", this.config.getUsername());
        connectionProps.put("password", this.config.getPassword());

        connectionProps.putAll(this.config.getConnectionProperties());

        connection = DriverManager.getConnection("jdbc:"+this.config.getDriver()+"://"+this.config.getUrl()+"/"+this.config.getDatabase(), connectionProps) ;
    }

    public static DataSource getInstance() {
        return dataBase;
    }

    public static DataSource getInstance(DataBaseConfig config) throws ClassNotFoundException, SQLException {
        if(dataBase==null) {
            dataBase = new DataSource(config);
        }
        return dataBase;
    }

    public Connection getConnection() {
        return connection;
    }

    public DataBaseConfig getConfig() {
        return config;
    }
}
