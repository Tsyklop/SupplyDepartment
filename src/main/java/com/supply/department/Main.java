package com.supply.department;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;
import com.supply.department.controller.*;
import com.supply.department.dao.DataBase;
import com.supply.department.dao.DataSource;
import com.supply.department.datasource.config.DataBaseConfig;
import com.supply.department.excel.ExcelCreator;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicReference;

public class Main extends Application {

    private Stage stage;

    private Scene errorScene;
    private Scene windowScene;

    private DataBase dataBase;
    private DataSource dataSource;

    private WindowController windowController;

    public static final String SEPARATOR = "/";

    public static final String PROJECT_NAME = "supply";
    public static final String PROJECT_DATABASE_XML = "database.xml";

    public static final String USER_HOME = System.getProperty("user.home");

    public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

    private static Logger LOGGER = LogManager.getLogger(Main.class);

    @Override
    public void start(Stage primaryStage) throws Exception {

        ExcelCreator.getInstance();

        stage = primaryStage;

        stage.setTitle("Supply Department");

        AtomicReference<FXMLLoader> fxmlLoader = new AtomicReference<>(new FXMLLoader());

        fxmlLoader.get().setLocation(getFxml("loading"));

        stage.setScene(new Scene(fxmlLoader.get().load()));

        stage.show();

        Platform.runLater(() -> {

            try {

                File dataBaseConfigFile = new File(USER_HOME+SEPARATOR+PROJECT_NAME+SEPARATOR+PROJECT_DATABASE_XML);

                if(!dataBaseConfigFile.exists()) {
                    FileUtils.write(dataBaseConfigFile, IOUtils.resourceToString(PROJECT_DATABASE_XML, DEFAULT_CHARSET, getClass().getClassLoader()), DEFAULT_CHARSET);
                }

                try (InputStream dataBaseXml = new FileInputStream(dataBaseConfigFile)) {
                    if(dataBaseXml!=null) {

                        try {

                            JAXBContext jaxbContext = JAXBContext.newInstance(DataBaseConfig.class);
                            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

                            dataSource = DataSource.getInstance((DataBaseConfig) jaxbUnmarshaller.unmarshal(dataBaseXml));

                            dataBase = new DataBase(dataSource.getConnection());

                            fxmlLoader.set(new FXMLLoader());

                            fxmlLoader.get().setLocation(getFxml("window"));

                            windowScene = new Scene(fxmlLoader.get().load());

                            windowController = fxmlLoader.get().getController();
                            windowController.setMain(this);

                            stage.hide();
                            stage.setScene(windowScene);
                            stage.show();

                        } catch (JAXBException|ClassNotFoundException e) {
                            LOGGER.error("EXCEPTION", e);
                            showError("Настройка базы данных не корректна. Проверьте файл database.xml по пути: "+dataBaseConfigFile.getAbsolutePath(), true);
                        } catch (CommunicationsException e) {
                            LOGGER.error("CommunicationsException", e);
                            showError("Ошибка подключения к базе данных: \n\t- Программа XAMPP не запущена", true);
                        } catch (MySQLSyntaxErrorException e) {
                            LOGGER.error("MySQLSyntaxErrorException", e);
                            showError("Ошибка подключения к базе данных: \n\t- База данных не создана", true);
                        }

                    }
                }

            } catch (Exception e) {
                LOGGER.error("ERROR", e);
                showError("Ошибка: "+e.getMessage(), true);
            }

        });

    }

    public DataBase getDataBase() {
        return dataBase;
    }

    public void showRequestAddModal() {

        try {

            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(getFxml("modal/requestAdd"));

            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Новая Заявка");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.stage);

            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            RequestAddController controller = loader.getController();
            controller.setStage(dialogStage);
            controller.setMain(this);

            dialogStage.showAndWait();

            if(controller.isStatus()) {
                windowController.updateTables();
            }

        } catch (Exception e) {
            LOGGER.error("ERROR", e);
            showError("Ошибка открытия окна: "+e.getMessage());
        }

    }

    public void showComingAddModal() {

        try {

            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(getFxml("modal/comingAdd"));

            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Новый приход");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.stage);

            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ComingAddController controller = loader.getController();
            controller.setMain(this);
            controller.setStage(dialogStage);

            dialogStage.showAndWait();

            if(controller.isStatus()) {
                windowController.updateTables();
            }

        } catch (Exception e) {
            LOGGER.error("ERROR", e);
            showError("Ошибка открытия окна: "+e.getMessage());
        }

    }

    public void showExpenditureAddModal() {

        try {

            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(getFxml("modal/expenditureAdd"));

            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Новый приход");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.stage);

            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ExpenditureAddController controller = loader.getController();
            controller.setMain(this);
            controller.setStage(dialogStage);

            dialogStage.showAndWait();

            if(controller.isStatus()) {
                windowController.updateTables();
            }

        } catch (Exception e) {
            LOGGER.error("ERROR", e);
            showError("Ошибка открытия окна: "+e.getMessage());
        }

    }

    public void showReportCreateModal() {

        try {

            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(getFxml("modal/reportCreate"));

            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Создать отчет");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.stage);

            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ReportCreateController controller = loader.getController();
            controller.setMain(this);
            controller.setStage(dialogStage);

            dialogStage.showAndWait();

            /*if(controller.isStatus()) {
                windowController.updateTables();
            }*/

        } catch (Exception e) {
            LOGGER.error("ERROR", e);
            showError("Ошибка открытия окна: "+e.getMessage());
        }

    }

    public void showCatalogRequests() {

        try {

            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(getFxml("modal/requests"));

            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();

            dialogStage.setTitle("Заявки");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.stage);

            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            RequestsController controller = loader.getController();
            controller.setMain(this);

            dialogStage.showAndWait();

        } catch (Exception e) {
            LOGGER.error("ERROR", e);
            showError("Ошибка открытия окна: " + e.getMessage());
        }

    }

    public void showCatalogMaterials() {

        try {

            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(getFxml("modal/materials"));

            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();

            dialogStage.setTitle("Материалы");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.stage);

            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            MaterialsController controller = loader.getController();
            controller.setMain(this);

            dialogStage.showAndWait();

        } catch (Exception e) {
            LOGGER.error("ERROR", e);
            showError("Ошибка открытия окна: " + e.getMessage());
        }

    }

    public void showError(String message) {
        showError(message, false);
    }

    public void showError(String message, boolean fatal) {
        if(fatal) {
            stage.close();
        }

        Alert alert = new Alert(Alert.AlertType.ERROR);

        alert.setTitle("Ошибка");

        alert.setHeaderText(null);

        alert.setContentText(message);

        alert.showAndWait();

        if(fatal) {
            Platform.exit();
        }

    }

    public void showSuccess(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Успешно!");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static URL getFxml(String name) {
        return Main.class.getClassLoader().getResource("fxml/"+name+".fxml");
    }

    public static LocalDate dateToLocalDate(String dateString){
        return LocalDate.parse(dateString, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

}
