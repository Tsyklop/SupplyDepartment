package com.supply.department.datasource.config;

import com.supply.department.datasource.adapter.HashMapAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@XmlRootElement(name = "database")
public class DataBaseConfig {

    @XmlElement
    private String url;

    @XmlElement
    private String driver;

    @XmlElement
    private String database;

    @XmlElement
    private String username;

    @XmlElement
    private String password;

    @XmlElement
    private String driverClassName;

    @XmlElement
    @XmlJavaTypeAdapter(HashMapAdapter.class)
    private HashMap<String, String> connectionProperties;

    public String getUrl() {
        return url;
    }

    public String getDriver() {
        return driver;
    }

    public String getDatabase() {
        return database;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public HashMap<String, String> getConnectionProperties() {
        return connectionProperties;
    }

    public String connectionPropertiesToString() {
        StringBuilder connectionProperties = new StringBuilder("");
        for(Map.Entry<String, String> entry: this.connectionProperties.entrySet()) {
            connectionProperties.append(entry.getKey()).append("=").append(entry.getValue()).append(";");
        }
        return connectionProperties.toString();
    }

    @Override
    public String toString() {
        return "DataSourceConfig{" +
                "url='" + url + '\'' +
                ", driver='" + driver + '\'' +
                ", database='" + database + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", driverClassName='" + driverClassName + '\'' +
                ", connectionProperties=" + connectionProperties +
                '}';
    }
}
