package com.supply.department;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

public class ApachePOIExcelRead {

    private static final String FILE_NAME = "E:\\IDEA\\SupplyDepartment\\form.xlsx";

    public static void main(String[] args) {

        try (FileInputStream excelFile = new FileInputStream(new File(FILE_NAME))) {

            Workbook workbook = new XSSFWorkbook(excelFile);

            Sheet datatypeSheet = workbook.getSheetAt(0);

            int rowNum = 1;
            for (Row currentRow : datatypeSheet) {

                StringBuilder rowString = new StringBuilder(rowNum+": ");

                for (Cell currentCell : currentRow) {

                    CellStyle style = currentCell.getCellStyle();

                    String val = currentCell.getCellTypeEnum() == CellType.NUMERIC? String.valueOf(currentCell.getNumericCellValue()) :currentCell.getStringCellValue();

//                    rowString.append(" ");
                    rowString.append(val.isEmpty()?style.getBorderBottomEnum()==BorderStyle.THIN?"_":" ":val);
//                    rowString.append(" ");

                    /*//getCellTypeEnum shown as deprecated for version 3.15
                    //getCellTypeEnum ill be renamed to getCellType starting from version 4.0
                    if (currentCell.getCellTypeEnum() == CellType.STRING) {
                        System.out.print(currentCell.getStringCellValue() + "--");
                    } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                        System.out.print(currentCell.getNumericCellValue() + "--");
                    }*/

                }
                System.out.println(rowString.toString());
                System.out.println();
                rowNum++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
