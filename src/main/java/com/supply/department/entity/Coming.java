package com.supply.department.entity;

public class Coming extends Data {

    private String idDoc; // № приходнго документа
    private String powerOfAttorney; // № и дата доверенности

    private String provider;

    private String extraOptions;

    private long freeCount;

    public Coming() {}

    public Coming(long id, String date, String fio, String name, String materialGroup, String unit,
                  long count, double price, double amount, String purchaseCode, String accountingGroup, String idDoc, String powerOfAttorney, String provider, String extraOptions) {
        super(id, date, fio, name, materialGroup, unit, count, price, amount, purchaseCode, accountingGroup);
        this.idDoc = idDoc;
        this.powerOfAttorney = powerOfAttorney;
        this.provider = provider;
        this.extraOptions = extraOptions;
    }

    public String getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(String idDoc) {
        this.idDoc = idDoc;
    }

    public String getPowerOfAttorney() {
        return powerOfAttorney;
    }

    public void setPowerOfAttorney(String powerOfAttorney) {
        this.powerOfAttorney = powerOfAttorney;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getExtraOptions() {
        return extraOptions;
    }

    public void setExtraOptions(String extraOptions) {
        this.extraOptions = extraOptions;
    }

    @Override
    public String toString() {
        return "Coming{" +
                "idDoc='" + idDoc + '\'' +
                ", powerOfAttorney='" + powerOfAttorney + '\'' +
                ", provider='" + provider + '\'' +
                ", extraOptions='" + extraOptions + '\'' +
                ", Data='" + super.toString() + '\'' +
                '}';
    }

    public long getFreeCount() {
        return freeCount;
    }

    public void setFreeCount(long freeCount) {
        this.freeCount = freeCount;
    }
}
