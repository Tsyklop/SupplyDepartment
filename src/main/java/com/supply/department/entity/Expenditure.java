package com.supply.department.entity;

public class Expenditure extends Data {

    private long comingId;

    private String basis;

    private String department;

    public Expenditure() {}

    public Expenditure(long id, long comingId, String date, String fio, String name, String materialGroup, String unit, long count, double price, double amount, String purchaseCode, String accountingGroup, String basis, String department) {
        super(id, date, fio, name, materialGroup, unit, count, price, amount, purchaseCode, accountingGroup);
        this.comingId = comingId;
        this.basis = basis;
        this.department = department;
    }

    public String getBasis() {
        return basis;
    }

    public void setBasis(String basis) {
        this.basis = basis;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public long getComingId() {
        return comingId;
    }

    public void setComingId(long comingId) {
        this.comingId = comingId;
    }
}
