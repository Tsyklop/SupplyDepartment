package com.supply.department.entity;

public class Remainder extends Coming {


    public Remainder() { }

    public Remainder(long id, String date, String fio, String name, String materialGroup, String unit, long count, double price, double amount, String purchaseCode, String accountingGroup, String idDoc, String powerOfAttorney, String provider, String extraOptions) {
        super(id, date, fio, name, materialGroup, unit, count, price, amount, purchaseCode, accountingGroup, idDoc, powerOfAttorney, provider, extraOptions);
    }
}
