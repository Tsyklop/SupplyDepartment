package com.supply.department.entity;

public abstract class Data {

    private long id;

    private String date;

    private String fio;
    private String name;
    private String materialGroup;
    private String unit;

    private long count;
    private double price;
    private double amount;

    private String purchaseCode;
    private String accountingGroup;

    public Data() {}

    public Data(long id, String date, String fio, String name, String materialGroup, String unit, long count, double price, double amount, String purchaseCode, String accountingGroup) {
        this.id = id;
        this.date = date;
        this.fio = fio;
        this.name = name;
        this.materialGroup = materialGroup;
        this.unit = unit;
        this.count = count;
        this.price = price;
        this.amount = amount;
        this.purchaseCode = purchaseCode;
        this.accountingGroup = accountingGroup;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaterialGroup() {
        return materialGroup;
    }

    public void setMaterialGroup(String materialGroup) {
        this.materialGroup = materialGroup;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPurchaseCode() {
        return purchaseCode;
    }

    public void setPurchaseCode(String purchaseCode) {
        this.purchaseCode = purchaseCode;
    }

    public String getAccountingGroup() {
        return accountingGroup;
    }

    public void setAccountingGroup(String accountingGroup) {
        this.accountingGroup = accountingGroup;
    }

    @Override
    public String toString() {
        return "Data{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", fio='" + fio + '\'' +
                ", name='" + name + '\'' +
                ", materialGroup='" + materialGroup + '\'' +
                ", unit='" + unit + '\'' +
                ", count=" + count +
                ", price=" + price +
                ", amount=" + amount +
                ", purchaseCode='" + purchaseCode + '\'' +
                ", accountingGroup='" + accountingGroup + '\'' +
                '}';
    }
}
