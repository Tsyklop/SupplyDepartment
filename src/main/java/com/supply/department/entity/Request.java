package com.supply.department.entity;

public class Request {

    private long id;

    private String department;

    private String info;

    public Request() { }

    public Request(long id, String department, String info) {
        this.id = id;
        this.department = department;
        this.info = info;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
