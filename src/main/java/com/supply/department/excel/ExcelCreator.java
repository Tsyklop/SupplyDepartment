package com.supply.department.excel;

import com.supply.department.entity.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.TemporalField;
import java.util.Calendar;
import java.util.Date;

public class ExcelCreator {

    private Workbook workbook;

    private boolean ready = false;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private static Logger LOGGER = LogManager.getLogger(ExcelCreator.class);

    private static ExcelCreator ourInstance = new ExcelCreator();

    public static ExcelCreator getInstance() {
        return ourInstance;
    }

    private ExcelCreator() {
        try (InputStream xlsxTemplate = ExcelCreator.class.getClassLoader().getResourceAsStream("form.xlsx")) {

            if(xlsxTemplate==null) {
                LOGGER.error("XLSX FILE NOT FOUND");
            } else {
                LOGGER.info("XLSX FILE FOUND");
                try {

                    workbook = new XSSFWorkbook(xlsxTemplate);
                    ready = true;

                } catch (IOException e) {
                    LOGGER.error("ERROR", e);
                }
            }

        } catch (IOException e) {
            LOGGER.error("ERROR", e);
        }
    }

    public boolean isReady() {
        return ready;
    }

    public boolean createReport(Data data, File destinationFolder) {
        try {
            Sheet sheet = workbook.getSheetAt(0);

            // WRITE ID

            Row idRow = sheet.getRow(2);
            Cell idCell = idRow.getCell(21);
            idCell.setCellType(CellType.STRING);
            idCell.setCellValue(data.getId());

            // WRITE DATE

            Calendar c = Calendar.getInstance();

            c.setTime(dateFormat.parse(data.getDate()));

            Row dayRow = sheet.getRow(4);
            Cell dayCell = dayRow.getCell(20);
            dayCell.setCellType(CellType.STRING);
            dayCell.setCellValue(c.get(Calendar.DAY_OF_MONTH));

            Row dateRow = sheet.getRow(4);
            Cell dateCell = dateRow.getCell(22);
            dateCell.setCellType(CellType.STRING);
            dateCell.setCellValue(c.get(Calendar.YEAR));

            // WRITE FIO

            Row fioRow = sheet.getRow(9);
            Cell fioCell = fioRow.getCell(4);
            fioCell.setCellType(CellType.STRING);
            fioCell.setCellValue(data.getFio());

            // WRITE ITEM INFO

            Row itemIdRow = sheet.getRow(18);
            Cell itemIdCell = itemIdRow.getCell(0);
            itemIdCell.setCellType(CellType.STRING);
            itemIdCell.setCellValue(data.getId());

            Row itemNameRow = sheet.getRow(18);
            Cell itemNameCell = itemNameRow.getCell(1);
            itemNameCell.setCellType(CellType.STRING);
            itemNameCell.setCellValue(data.getName());

            Row itemUnitRow = sheet.getRow(18);
            Cell itemUnitCell = itemUnitRow.getCell(14);
            itemUnitCell.setCellType(CellType.STRING);
            itemUnitCell.setCellValue(data.getUnit());

            Row itemCountRow = sheet.getRow(18);
            Cell itemCountCell = itemCountRow.getCell(17);
            itemCountCell.setCellType(CellType.STRING);
            itemCountCell.setCellValue(data.getCount());

            Row itemPriceRow = sheet.getRow(18);
            Cell itemPriceCell = itemPriceRow.getCell(20);
            itemPriceCell.setCellType(CellType.STRING);
            itemPriceCell.setCellValue(data.getPrice());

            Row itemAmountRow = sheet.getRow(18);
            Cell itemAmountCell = itemAmountRow.getCell(24);
            itemAmountCell.setCellType(CellType.STRING);
            itemAmountCell.setCellValue(data.getCount()*data.getPrice());

            Row itemAmountAllRow = sheet.getRow(25);
            Cell itemAmountAllCell = itemAmountAllRow.getCell(24);
            itemAmountAllCell.setCellType(CellType.STRING);
            itemAmountAllCell.setCellValue((long) itemAmountCell.getNumericCellValue());

            FileOutputStream out = new FileOutputStream(new File(destinationFolder+"/"+new Date().getTime()+".xlsx"));
            workbook.write(out);
            out.close();

            return true;

        } catch (Exception e) {
            LOGGER.error("ERROR", e);
        }
        return false;
    }

}
