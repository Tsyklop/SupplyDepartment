package com.supply.department.controller;

import com.supply.department.Main;
import javafx.stage.Stage;

public abstract class ControllerImpl implements Controller {

    private Main main;

    private Stage stage;

    ControllerImpl() { }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void initialize() {

    }

}
