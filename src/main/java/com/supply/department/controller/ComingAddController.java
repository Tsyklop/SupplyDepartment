package com.supply.department.controller;

import com.supply.department.Main;
import com.supply.department.entity.Coming;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class ComingAddController extends ControllerImpl {
    
    @FXML
    private DatePicker date;

    @FXML
    private TextField idDoc;

    @FXML
    private TextField powerOfAttorney;

    @FXML
    private TextField fio;

    @FXML
    private TextField provider;

    @FXML
    private TextField name;

    @FXML
    private TextField materialGroup;

    @FXML
    private TextField unit;

    @FXML
    private TextField count;

    @FXML
    private TextField price;

    @FXML
    private TextField amount;

    @FXML
    private TextField purchaseCode;

    @FXML
    private TextField accountingGroup;

    @FXML
    private TextArea extraOptions;

    private boolean status = false;

    private static Logger LOGGER = LogManager.getLogger(ComingAddController.class);

    @FXML
    public void initialize() {

        count.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                amount.setText(String.valueOf(Long.parseLong(newValue)*Double.parseDouble(price.getText())));
            } catch (Exception e) {
                amount.setText("0");
            }
        });

        price.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                amount.setText(String.valueOf(Long.parseLong(count.getText())*Double.parseDouble(newValue)));
            } catch (Exception e) {
                amount.setText("0");
            }
        });

    }

    @FXML
    public void onSave() {

        try {

            count.setText(count.getText().replaceAll("[^\\d.]", ""));
            price.setText(price.getText().replaceAll("[^\\d.]", ""));
            amount.setText(amount.getText().replaceAll("[^\\d.]", ""));

            Coming coming = new Coming(
                    -1,
                    date.getValue().toString(),
                    fio.getText(),
                    name.getText(),
                    materialGroup.getText(),
                    unit.getText(),
                    Long.parseLong(count.getText()),
                    Double.parseDouble(price.getText()),
                    Double.parseDouble(amount.getText()),
                    purchaseCode.getText(),
                    accountingGroup.getText(),
                    idDoc.getText(),
                    powerOfAttorney.getText(),
                    provider.getText(),
                    extraOptions.getText()
            );

            LOGGER.info(coming.toString());

            if(getMain().getDataBase().comingAdd(coming)) {
                status = true;
                getMain().showSuccess("Приход успешно добавлен!");
                getStage().close();
            } else {
                getMain().showError("Ошибка добавления прихода");
            }

        } catch (NumberFormatException e) {
            LOGGER.error("", e);
            getMain().showError("Ошибка. Введите корректные данные");
        } catch (SQLException e) {
            LOGGER.error("", e);
            getMain().showError("Ошибка базы данных. Смотрите логи");
        }

    }
    
    public boolean isStatus() {
        return status;
    }
}
