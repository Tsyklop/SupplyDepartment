package com.supply.department.controller;

import com.supply.department.Main;
import com.supply.department.entity.Request;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class RequestAddController extends ControllerImpl {
    
    @FXML
    private TextField department;

    @FXML
    private TextArea info;

    private boolean status = false;

    private static Logger LOGGER = LogManager.getLogger(RequestAddController.class);

    @FXML
    public void initialize() {
        if(getMain()!=null) {

        }
    }

    @FXML
    public void onSave() {

        if(!department.getText().isEmpty()) {
            if(!info.getText().isEmpty()) {

                Request request = new Request(-1, department.getText(), info.getText());

                try {
                    if(getMain().getDataBase().requestsAdd(request)) {
                        this.status = true;
                        getMain().showSuccess("Расход успешно добавлен!");
                        getStage().close();
                    } else {
                        getMain().showError("Ошибка добавления заявки");
                    }
                } catch (SQLException e) {
                    LOGGER.error("SQL ERROR", e);
                    getMain().showError("Ошибка базы данных: "+e.getMessage());
                }

            } else {
                getMain().showError("Введите описание заявки");
            }
        } else {
            getMain().showError("Введите название отдела");
        }

    }

    @Override
    public void setMain(Main main) {
        super.setMain(main);
        initialize();
    }

    public boolean isStatus() {
        return status;
    }

}
