package com.supply.department.controller;

import com.supply.department.Main;
import com.supply.department.entity.Coming;
import com.supply.department.entity.Expenditure;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static com.supply.department.Main.dateToLocalDate;

public class ExpenditureAddController extends ControllerImpl {
    
    private Coming selectedComing;

    @FXML
    private ComboBox<Coming> comings;

    @FXML
    private TextField basis;

    @FXML
    private TextField department;

    @FXML
    private DatePicker date;

    @FXML
    private Label fio;

    @FXML
    private Label name;

    @FXML
    private Label materialGroup;

    @FXML
    private Label unit;

    @FXML
    private TextField count;

    @FXML
    private Label price;

    @FXML
    private Label amount;

    private boolean status = false;

    private static Logger LOGGER = LogManager.getLogger(ExpenditureAddController.class);

    @FXML
    public void initialize() {
        if(getMain()!=null) {

            try {

                count.textProperty().addListener((observable, oldValue, newValue) -> {
                    if(selectedComing!=null) {
                        try {
                            amount.setText(String.valueOf(Long.parseLong(newValue)*Double.parseDouble(price.getText())));
                        } catch (Exception e) {
                            count.setText("0");
                        }
                    }
                });

                comings.setCellFactory(new Callback<ListView<Coming>,ListCell<Coming>>(){
                    @Override
                    public ListCell<Coming> call(ListView<Coming> comingListView){
                        return new ListCell<Coming>(){
                            @Override
                            protected void updateItem(Coming item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item == null || empty) {
                                    setGraphic(null);
                                } else {
                                    setText(item.getId()+" "+item.getName()+" "+item.getDate()+" "+item.getFio());
                                }
                            }
                        } ;
                    }
                });
                //selected value showed in combo box
                comings.setConverter(new StringConverter<Coming>() {
                    @Override
                    public String toString(Coming coming) {
                        if (coming == null){
                            return null;
                        } else {
                            return coming.getId()+" "+coming.getName()+" "+coming.getDate()+" "+coming.getFio();
                        }
                    }
                    @Override
                    public Coming fromString(String userId) {
                        return null;
                    }
                });

                comings.valueProperty().addListener(new ChangeListener<Coming>() {
                    @Override
                    public void changed(ObservableValue<? extends Coming> observable, Coming oldValue, Coming newValue) {
                        onComingChanged(newValue);
                    }
                });

                comings.setItems(FXCollections.observableList(getMain().getDataBase().comingGetAllWithExpenditure().stream().filter(coming -> coming.getFreeCount()>0).collect(Collectors.toList())));
                comings.getSelectionModel().selectFirst();

            } catch (SQLException e) {
                LOGGER.error("SQL ERROR", e);
                getMain().showError("Ошибка базы данных: "+e.getMessage());
                if(getStage()!=null){getStage().close();};
            }

        }
    }

    @FXML
    public void onSave() {
        if(selectedComing!=null) {
            if(!basis.getText().isEmpty()) {
                if(!department.getText().isEmpty()) {

                    count.setText(count.getText().replaceAll("[^\\d.]", ""));

                    long eCount = Long.parseLong(count.getText());

                    if(eCount>0) {
                        if(eCount<=selectedComing.getFreeCount()) {

                            Expenditure expenditure = new Expenditure(-1, selectedComing.getId(),
                                    date.getValue().toString(), selectedComing.getFio(), selectedComing.getName(), selectedComing.getMaterialGroup(), selectedComing.getUnit(), eCount, selectedComing.getPrice(), eCount*selectedComing.getPrice(),
                                    selectedComing.getPurchaseCode(), selectedComing.getAccountingGroup(), basis.getText(), department.getText());

                            try {
                                if(getMain().getDataBase().expenditureAdd(expenditure)) {
                                    status = true;
                                    getMain().showSuccess("Расход успешно добавлен!");
                                } else {
                                    getMain().showError("Ошибка добавления расхода");
                                }
                            } catch (SQLException e) {
                                LOGGER.error("SQL ERROR", e);
                                getMain().showError("Ошибка базы данных: "+e.getMessage());
                            }

                        } else {
                            getMain().showError("Нет такого количества товара "+selectedComing.getName()+". Имеется "+selectedComing.getFreeCount());
                        }
                    } else {
                        getMain().showError("Введите количество требуемого довара");
                    }

                } else {
                    getMain().showError("Введите отдел");
                }
            } else {
                getMain().showError("Введите основание");
            }
        } else {
            getMain().showError("Выберите приход");
        }
    }

    private void onComingChanged(Coming coming) {

        selectedComing = coming;

        date.setValue(dateToLocalDate(coming.getDate()));

        fio.setText(coming.getFio());

        name.setText(coming.getName());

        materialGroup.setText(coming.getMaterialGroup());

        unit.setText(coming.getUnit());

        count.setText("0");
        amount.setText("0");
        price.setText(String.valueOf(coming.getPrice()));

    }

    @Override
    public void setMain(Main main) {
        super.setMain(main);
        initialize();
    }

    public boolean isStatus() {
        return status;
    }
}
