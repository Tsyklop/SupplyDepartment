package com.supply.department.controller;

import com.supply.department.Main;
import com.supply.department.entity.Coming;
import com.supply.department.entity.Expenditure;
import com.supply.department.entity.Remainder;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.stream.Collectors;

public class WindowController extends ControllerImpl {

    @FXML
    private TableView<Coming> comings;

    @FXML
    private TableView<Expenditure> expenditures;

    @FXML
    private TableView<Remainder> remains;

    @FXML
    private TableColumn<Coming, Long> comingColumnId;

    @FXML
    private TableColumn<Coming, String> comingColumnDate;

    @FXML
    private TableColumn<Coming, String> comingColumnIdDoc;

    @FXML
    private TableColumn<Coming, String> comingColumnPowerOfAttorney;

    @FXML
    private TableColumn<Coming, String> comingColumnFio;

    @FXML
    private TableColumn<Coming, String> comingColumnProvider;

    @FXML
    private TableColumn<Coming, String> comingColumnName;

    @FXML
    private TableColumn<Coming, String> comingColumnMaterialGroup;

    @FXML
    private TableColumn<Coming, String> comingColumnUnit;

    @FXML
    private TableColumn<Coming, Long> comingColumnCount;

    @FXML
    private TableColumn<Coming, Double> comingColumnPrice;

    @FXML
    private TableColumn<Coming, Double> comingColumnAmount;

    @FXML
    private TableColumn<Coming, String> comingColumnPurchaseCode;

    @FXML
    private TableColumn<Coming, String> comingColumnAccountingGroup;

    @FXML
    private TableColumn<Coming, String> comingColumnExtraOptions;

    @FXML
    private TableColumn<Expenditure, Long> expenditureColumnId;

    @FXML
    private TableColumn<Expenditure, String> expenditureColumnDate;

    @FXML
    private TableColumn<Expenditure, String> expenditureColumnBasis;

    @FXML
    private TableColumn<Expenditure, String> expenditureColumnDepartment;

    @FXML
    private TableColumn<Expenditure, String> expenditureColumnFio;

    @FXML
    private TableColumn<Expenditure, String> expenditureColumnName;

    @FXML
    private TableColumn<Expenditure, String> expenditureColumnMaterialGroup;

    @FXML
    private TableColumn<Expenditure, String> expenditureColumnUnit;

    @FXML
    private TableColumn<Expenditure, Long> expenditureColumnCount;

    @FXML
    private TableColumn<Expenditure, Double> expenditureColumnPrice;

    @FXML
    private TableColumn<Expenditure, Double> expenditureColumnAmount;

    @FXML
    private TableColumn<Expenditure, String> expenditureColumnPurchaseCode;

    @FXML
    private TableColumn<Expenditure, String> expenditureColumnAccountingGroup;

    @FXML
    private TableColumn<Remainder, Long> remainderColumnId;

    @FXML
    private TableColumn<Remainder, String> remainderColumnDate;

    @FXML
    private TableColumn<Remainder, String> remainderColumnIdDoc;

    @FXML
    private TableColumn<Remainder, String> remainderColumnPowerOfAttorney;

    @FXML
    private TableColumn<Remainder, String> remainderColumnFio;

    @FXML
    private TableColumn<Remainder, String> remainderColumnProvider;

    @FXML
    private TableColumn<Remainder, String> remainderColumnName;

    @FXML
    private TableColumn<Remainder, String> remainderColumnMaterialGroup;

    @FXML
    private TableColumn<Remainder, String> remainderColumnUnit;

    @FXML
    private TableColumn<Remainder, Long> remainderColumnCount;

    @FXML
    private TableColumn<Remainder, Double> remainderColumnPrice;

    @FXML
    private TableColumn<Remainder, Double> remainderColumnAmount;

    @FXML
    private TableColumn<Remainder, Long> remainderColumnFree;

    @FXML
    private TableColumn<Remainder, String> remainderColumnPurchaseCode;

    @FXML
    private TableColumn<Remainder, String> remainderColumnAccountingGroup;

    @FXML
    private TableColumn<Remainder, String> remainderColumnExtraOptions;

    @FXML
    private Label rowsCount;

    @FXML
    private Tab comingTab;

    @FXML
    private Tab expenditureTab;

    @FXML
    private Tab remainsTab;

    private static Logger LOGGER = LogManager.getLogger(WindowController.class);

    public WindowController() {}

    @Override
    public void initialize() {

        comingColumnId.setCellValueFactory(cellData -> new SimpleLongProperty(cellData.getValue().getId()).asObject());
        comingColumnDate.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDate()));
        comingColumnIdDoc.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getIdDoc()));
        comingColumnPowerOfAttorney.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPowerOfAttorney()));
        comingColumnFio.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFio()));
        comingColumnProvider.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getProvider()));
        comingColumnName.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        comingColumnMaterialGroup.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getMaterialGroup()));
        comingColumnUnit.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getUnit()));
        comingColumnCount.setCellValueFactory(cellData -> new SimpleLongProperty(cellData.getValue().getCount()).asObject());
        comingColumnPrice.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getPrice()).asObject());
        comingColumnAmount.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getAmount()).asObject());
        comingColumnPurchaseCode.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPurchaseCode()));
        comingColumnAccountingGroup.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAccountingGroup()));
        comingColumnExtraOptions.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getExtraOptions()));

        expenditureColumnId.setCellValueFactory(cellData -> new SimpleLongProperty(cellData.getValue().getId()).asObject());
        expenditureColumnDate.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDate()));
        expenditureColumnBasis.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getBasis()));
        expenditureColumnDepartment.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDepartment()));
        expenditureColumnDate.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDate()));
        expenditureColumnFio.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFio()));
        expenditureColumnName.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        expenditureColumnMaterialGroup.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getMaterialGroup()));
        expenditureColumnUnit.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getUnit()));
        expenditureColumnCount.setCellValueFactory(cellData -> new SimpleLongProperty(cellData.getValue().getCount()).asObject());
        expenditureColumnPrice.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getPrice()).asObject());
        expenditureColumnAmount.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getAmount()).asObject());
        expenditureColumnPurchaseCode.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPurchaseCode()));
        expenditureColumnAccountingGroup.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAccountingGroup()));

        remainderColumnId.setCellValueFactory(cellData -> new SimpleLongProperty(cellData.getValue().getId()).asObject());
        remainderColumnDate.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDate()));
        remainderColumnIdDoc.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getIdDoc()));
        remainderColumnPowerOfAttorney.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPowerOfAttorney()));
        remainderColumnFio.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFio()));
        remainderColumnProvider.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getProvider()));
        remainderColumnName.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        remainderColumnMaterialGroup.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getMaterialGroup()));
        remainderColumnUnit.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getUnit()));
        remainderColumnCount.setCellValueFactory(cellData -> new SimpleLongProperty(cellData.getValue().getCount()).asObject());
        remainderColumnPrice.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getPrice()).asObject());
        remainderColumnAmount.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getFreeCount()*cellData.getValue().getPrice()).asObject());
        remainderColumnFree.setCellValueFactory(cellData -> new SimpleLongProperty(cellData.getValue().getFreeCount()).asObject());
        remainderColumnPurchaseCode.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPurchaseCode()));
        remainderColumnAccountingGroup.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAccountingGroup()));
        remainderColumnExtraOptions.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getExtraOptions()));

    }

    @FXML
    public void onExit() {
        Platform.exit();
    }

    @FXML
    public void onCreateRequest() {
        getMain().showRequestAddModal();
    }

    @FXML
    public void onCreateComing() {
        getMain().showComingAddModal();
    }

    @FXML
    public void onCreateExpenditure() {
        getMain().showExpenditureAddModal();
    }

    @FXML
    public void onCreateReport() {
        getMain().showReportCreateModal();
    }

    @FXML
    public void onOpenCatalogRequests() {
        getMain().showCatalogRequests();
    }

    @FXML
    public void onOpenCatalogMaterials() {
        getMain().showCatalogMaterials();
    }

    @FXML
    public void onOpenLogFolder() throws IOException {
        Desktop.getDesktop().open(new File(System.getProperty("user.home")+"/supply"));
    }

    @FXML
    public void onTabSelectionChanged(Event event) {
        if(getMain()!=null) {
            updateTables();
        }
    }

    public void updateTables() {
        try {
            if(comingTab.isSelected()) {
                fillComings();
            } else if(expenditureTab.isSelected()) {
                fillExpenditures();
            } else if(remainsTab.isSelected()) {
                fillRemains();
            }
        } catch (SQLException e) {
            LOGGER.error("SQL ERROR", e);
            getMain().showError("Ошибка базы данных: "+e.getMessage());
        }
    }

    private void tablesClear() {
        comings.getItems().clear();
        expenditures.getItems().clear();
        remains.getItems().clear();
    }

    private void fillComings() throws SQLException {
        tablesClear();
        comings.setItems(FXCollections.observableArrayList(getMain().getDataBase().comingGetAll()));
        rowsCount.setText(String.valueOf(comings.getItems().size()));
    }

    private void fillExpenditures() throws SQLException {
        tablesClear();
        expenditures.setItems(FXCollections.observableArrayList(getMain().getDataBase().expenditureGetAll()));
        rowsCount.setText(String.valueOf(expenditures.getItems().size()));
    }

    private void fillRemains() throws SQLException {
        tablesClear();
        remains.setItems(FXCollections.observableArrayList(getMain().getDataBase().remainsGetAll().stream().filter(remainder -> remainder.getFreeCount()>0).collect(Collectors.toList())));
        rowsCount.setText(String.valueOf(remains.getItems().size()));
    }

    @Override
    public void setMain(Main main) {
        super.setMain(main);
        try {
            fillComings();
        } catch (SQLException e) {
            LOGGER.error("SQL ERROR", e);
            getMain().showError("Ошибка базы данных: "+e.getMessage());
        }
    }



}
