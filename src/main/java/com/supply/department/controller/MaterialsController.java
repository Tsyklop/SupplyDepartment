package com.supply.department.controller;

import com.supply.department.Main;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class MaterialsController extends ControllerImpl {

    @FXML
    private ListView<String> materials;

    private static Logger LOGGER = LogManager.getLogger(MaterialsController.class);

    @FXML
    public void initialize() {
        if (getMain() != null) {

            materials.getItems().clear();

            try {

                getMain().getDataBase().materialsGetAll().stream().forEach(material -> {
                    if (material.getCount() > 0) {
                        materials.getItems().add(material.getName());
                    }
                });

            } catch (SQLException e) {
                LOGGER.error("SQL ERROR", e);
                getMain().showError("Ошибка базы данных: "+e.getMessage());
                if(getStage()!=null){getStage().close();};
            }

        }
    }

    @Override
    public void setMain(Main main){
        super.setMain(main);
        initialize();
    }
}
