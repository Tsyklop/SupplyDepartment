package com.supply.department.controller;

import com.supply.department.Main;
import com.supply.department.entity.Coming;
import com.supply.department.entity.Data;
import com.supply.department.excel.ExcelCreator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class ReportCreateController extends ControllerImpl {

    @FXML
    private RadioButton radioComing;

    @FXML
    private RadioButton radioExpenditure;

    @FXML
    private ComboBox<Data> objects;

    private static Logger LOGGER = LogManager.getLogger(ReportCreateController.class);

    @Override
    public void initialize() {
        if(getMain()!=null) {
            final ToggleGroup group = new ToggleGroup();

            radioComing.setToggleGroup(group);
            radioExpenditure.setToggleGroup(group);

            objects.setCellFactory(new Callback<ListView<Data>,ListCell<Data>>(){
                @Override
                public ListCell<Data> call(ListView<Data> comingListView){
                    return new ListCell<Data>(){
                        @Override
                        protected void updateItem(Data item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                            } else {
                                setText(item.getId()+" "+item.getName()+" "+item.getDate()+" "+item.getFio());
                            }
                        }
                    } ;
                }
            });

            objects.setConverter(new StringConverter<Data>() {
                @Override
                public String toString(Data data) {
                    if (data == null){
                        return null;
                    } else {
                        return data.getId()+" "+data.getName()+" "+data.getDate()+" "+data.getFio();
                    }
                }
                @Override
                public Coming fromString(String userId) {
                    return null;
                }
            });

            group.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
                if(group.getSelectedToggle() != null) {
                    try {
                        if(group.getSelectedToggle() == radioComing) {
                            objects.setItems(FXCollections.observableList(new ArrayList<>(getMain().getDataBase().comingGetAll())));
                        } else if(group.getSelectedToggle() == radioExpenditure) {
                            objects.setItems(FXCollections.observableList(new ArrayList<>(getMain().getDataBase().expenditureGetAll())));
                        }
                        objects.getSelectionModel().selectFirst();
                    } catch (SQLException e) {
                        LOGGER.error("SQL ERROR", e);
                        getMain().showError("Ошибка базы данных: "+e.getMessage());
                    }
                }
            });

            radioComing.setSelected(true);

        }
    }

    @Override
    public void setMain(Main main) {
        super.setMain(main);
        initialize();
    }

    @FXML
    public void onCreate() {

        ExcelCreator excelCreator = ExcelCreator.getInstance();

        if(excelCreator.isReady()) {

            final DirectoryChooser directoryChooser = new DirectoryChooser();

            final File selectedDirectory = directoryChooser.showDialog(getStage());

            if (selectedDirectory != null) {
                if(excelCreator.createReport(objects.getSelectionModel().getSelectedItem(), selectedDirectory.getAbsoluteFile())) {
                    getMain().showSuccess("Отчет создан");
                }
            }

        } else {
            getMain().showError("Шаблон EXCEL не найден");
        }

    }

}
