package com.supply.department.controller;

import javafx.fxml.FXML;

public interface Controller {

    @FXML
    void initialize();

}
