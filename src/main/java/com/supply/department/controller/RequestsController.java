package com.supply.department.controller;

import com.supply.department.Main;
import com.supply.department.entity.Request;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class RequestsController extends ControllerImpl {

    @FXML
    private TableView<Request> requests;

    @FXML
    private TableColumn<Request, Long> columnId;

    @FXML
    private TableColumn<Request, String> columnDepartment;

    @FXML
    private TableColumn<Request, String> columnInfo;

    private static Logger LOGGER = LogManager.getLogger(RequestsController.class);

    @Override
    public void initialize() {
        if(getMain()!=null) {

            requests.getItems().clear();

            columnId.setCellValueFactory(cellData -> new SimpleLongProperty(cellData.getValue().getId()).asObject());
            columnDepartment.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDepartment()));
            columnInfo.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getInfo()));

            try {
                getMain().getDataBase().requestsGetAll().stream().forEach(request -> {
                    requests.getItems().add(request);
                });
            } catch (SQLException e) {
                LOGGER.error("SQL ERROR", e);
                getMain().showError("Ошибка базы данных: "+e.getMessage());
                if(getStage()!=null) {
                    getStage().close();
                }
            }
        }
    }

    @Override
    public void setMain(Main main) {
        super.setMain(main);
        initialize();
    }

}
